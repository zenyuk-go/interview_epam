package open_weather_map

import "testing"

func TestConvertToWeatherResult(t *testing.T) {
	// arrange
	expectedTemperature := 27
	expectedWindSpeed := 1
	rawResponse := []byte(`{"coord":{"lon":151.2073,"lat":-33.8679},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],"base":"stations",
		"main":{"temp":26.55,"feels_like":26.55,"temp_min":25.4,"temp_max":27.28,"pressure":1008,"humidity":24},"visibility":10000,
		"wind":{"speed":0.89,"deg":337,"gust":2.24},"clouds":{"all":0},"dt":1631343582,"sys":{"type":2,"id":2018875,"country":"AU","sunrise":1631304034,"sunset":1631346217},"timezone":36000,"id":2147714,"name":"Sydney","cod":200}`)

	// act
	actual, err := convertToWeatherResult(rawResponse)

	// assert
	if err != nil {
		t.Fatal(err)
	}
	if actual.WindSpeed != expectedWindSpeed {
		t.Fatal("wind speed doesn't match")
	}
	if	actual.TemperatureDegrees != expectedTemperature {
		t.Fatal("temperature doesn't match")
	}
}
