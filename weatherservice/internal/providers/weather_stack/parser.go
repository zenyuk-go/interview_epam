package weather_stack

import (
	"encoding/json"
	"fmt"

	"zeny.uk/epam/internal/model"
)

type weatherStackResult struct {
	Current weatherStackResultCurrent `json:"current"`
}

type weatherStackResultCurrent struct {
	Temperature int `json:"temperature"`
	WindSpeed int `json:"wind_speed"`
}

func convertToWeatherResult(raw []byte) (*model.WeatherResult, error) {
	parsedResponse := weatherStackResult{}
	err := json.Unmarshal(raw, &parsedResponse)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling response, details: %v", err)
	}
	result := &model.WeatherResult{TemperatureDegrees: parsedResponse.Current.Temperature, WindSpeed: parsedResponse.Current.WindSpeed}
	return result, nil
}
